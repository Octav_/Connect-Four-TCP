#include "Token.hpp"

Token::Token(int x, int y, sf::Color c)
{
    pos.x = x;
    pos.y = y;

    circleShape.setFillColor(c);

    taken = false;
}


sf::CircleShape* Token::getCircleShape()
{
    return &circleShape;
}


void Token::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(circleShape);
}


sf::Vector2<unsigned short int>* Token::getPosition()
{
    return &pos;
}


bool Token::isTaken()
{
    return taken;
}


void Token::take(sf::Color t)
{
    circleShape.setFillColor(t);
    taken = true;
}



void Token::free()
{
    circleShape.setFillColor(sf::Color(33,49,72));
    taken = false;
}
