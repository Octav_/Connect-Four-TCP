#include "Game.hpp"

extern int errno;

Game::Game() : board(30)
{
    wait = true;
    isFirst = false;
    showText = false;

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    windowSize.x = 620;
    windowSize.y = 720;

    window.create(sf::VideoMode(windowSize.x, windowSize.y), "Connect Four de Gensthaler Octavian [IIB5]", sf::Style::Default, settings);
    window.setFramerateLimit(60);

    backgroundColor = sf::Color(51, 113, 172);

    if (!defaultFont.loadFromFile("arial.ttf"))
        cout << "[CLIENT] Eroare la incarcarea fontului arial.ttf" << endl;

    waitingText.setFont(defaultFont);
    waitingText.setString("Camera este momentan goala.\nSe asteapta al doilea jucator.");
    waitingText.setCharacterSize(20);
    waitingText.setPosition(sf::Vector2f(170, 600));
}



Game::~Game()
{
    close (sd);
}



void Game::run()
{
    while(window.isOpen())
    {
        // de aici incepe logica clientului
        pollEvents(&window);

        if(isFirst)
            board.hoverCheck(sf::Mouse::getPosition(window), sf::Color::Red);
        else
            board.hoverCheck(sf::Mouse::getPosition(window), sf::Color::Yellow);

        board.update();

        window.clear(backgroundColor); 
        draw(&window);
        window.display();

        if(wait)
        {
            int answer = waitForServerInput();

            switch(answer)
            {
                case -200:
                    waitingText.setString("Celalalt client s-a deconectat.");
                    wait = false;
                    showText = true;
                    close (sd);
                    break;

                case 100:
                    cout << "[CLIENT] Serverul a semnalizat ca jocul incepe" << endl;
                    break;

                case 101:
                    cout << "[CLIENT] Serverul a semnalizat ca sunt primul jucator." << endl;
                    waitingText.setString("Asteptam ca celalalt jucator sa faca o mutare...");
                    waitingText.setPosition(sf::Vector2f(100, 600));
                    isFirst = true;
                    wait = false;
                    break;

                case 102:
                    cout << "[CLIENT] Serverul a semnalizat ca nu sunt primul jucator." << endl;
                    waitingText.setString("Asteptam ca celalalt jucator sa faca o mutare...");
                    waitingText.setPosition(sf::Vector2f(100, 600));
                    isFirst = false;
                    wait = true;
                    break;

                case 103:
                    cout << "[CLIENT] Serverul a semnalizat ca e randul meu." << endl;
                    wait = false;
                    break;

                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    if(isFirst)
                        board.takeTokenOnColumn(answer, sf::Color::Yellow);
                    else
                        board.takeTokenOnColumn(answer, sf::Color::Red);

                    wait = false;
                    break;

                case 1000:
                case 1001:
                case 1002:
                case 1003:
                case 1004:
                case 1005:
                case 1006:
                    answer = answer % 1000;

                    if(isFirst)
                        board.takeTokenOnColumn(answer, sf::Color::Yellow);
                    else
                        board.takeTokenOnColumn(answer, sf::Color::Red);

                    waitingText.setString("Sorry! Ai pierdut");

                    wait = false;
                    showText = true;
                    break;
            }

            cout << endl;
        }
    }
}




void Game::pollEvents(sf::RenderWindow *window)
{
    sf::Event event;
    int answer;

    while (window -> pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                answer = -100;

                write (sd, &answer ,sizeof(int));
                close (sd);
                window -> close();
                break;

            case sf::Event::KeyReleased:
                if (event.key.code == sf::Keyboard::Escape)
                {
                    answer = -100;

                    write (sd, &answer ,sizeof(int));
                    close (sd);
                    window -> close();
                }
                break;

            case sf::Event::MouseButtonPressed:
                if (event.mouseButton.button == sf::Mouse::Left)
                {
                    int nr = board.getBoardPosX(sf::Mouse::getPosition(*window));
                    sf::Color toCheck;
                    wait = true;
                    
                    if(isFirst)
                        toCheck = sf::Color::Red;
                    else
                        toCheck = sf::Color::Yellow;

                    if(board.horizontalVictoryCheck(toCheck) || 
                        board.verticalVictoryCheck(toCheck) ||
                        board.diagonalVictoryCheck(toCheck))
                    {
                            nr += 1000;
                            waitingText.setString("FELICITARI! Ai castigat.");
                            showText = true;
                            wait = false;
                    }

                    if (write (sd,&nr,sizeof(int)) <= 0)
                        perror ("[client]Eroare la write() spre server.\n");

                    cout << "[CLIENT] Trimitem " << nr << " catre server " << endl; 

                    board.takeToken(sf::Mouse::getPosition(*window), toCheck); 

                }
                break;
        }
    }
}



void Game::draw(sf::RenderWindow *window)
{
    window -> draw(board);

    if(wait || showText)
        window -> draw(waitingText);
}



void Game::setPort(int p)
{
    port = p;
}



void Game::connectTo(char* ipAddr)
{
    if ((sd = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror ("Eroare la socket().\n");
        exit (EXIT_FAILURE);
    }
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(ipAddr);
    server.sin_port = htons(port);

    if (connect (sd, (struct sockaddr *) &server,sizeof (struct sockaddr)) == -1)
    {
        perror ("[client]Eroare la connect().\n");
        exit (EXIT_FAILURE);
    }
}



int Game::waitForServerInput()
{
    int payload;

    cout << "[CLIENT] Se asteapta input de la server..." << endl;

    // citirea raspunsului dat de server (apel blocant pina cind serverul raspunde)
    if (read (sd, &payload, sizeof(int)) < 0)
    {
        perror ("[client]Eroare la read() de la server.\n");
        exit (EXIT_FAILURE);
    }

    wait = false;
  
    cout <<  "[CLIENT] Mesajul primit este " << payload << endl;

    return payload;
}
