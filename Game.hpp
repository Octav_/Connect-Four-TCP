#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <netdb.h>
#include <cstring>
#include <iostream>
#include <string>

#include "Board.hpp"

using namespace std;

class Game
{
    private:
        sf::Vector2<unsigned short int> windowSize;
        sf::RenderWindow window;
        sf::Color backgroundColor;
        sf::Text waitingText;
        sf::Font defaultFont;

        Board board;

        int port;
        int sd;
        bool wait;
        bool showText;
        bool isFirst;
        struct sockaddr_in server;

    public:
        Game();
        ~Game();

        void run();
        void connectTo(char* ipAddr);
        void setPort(int p);
        void draw(sf::RenderWindow *window);
        void pollEvents(sf::RenderWindow *window);
        int waitForServerInput();
};
