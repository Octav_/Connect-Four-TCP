#include <SFML/Graphics.hpp>
#include <iostream>

class Token : public sf::Drawable
{
    private:
        sf::CircleShape circleShape;
        sf::Vector2<unsigned short int> pos;
        bool taken;

    public:
        ~Token() {}
        Token() {}
        Token(int x, int y, sf::Color c);

        sf::CircleShape* getCircleShape();
        sf::Vector2<unsigned short int>* getPosition();

        bool isTaken();
        void take(sf::Color t);
        void free();

    private:
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};
